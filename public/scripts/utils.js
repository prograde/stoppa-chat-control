

function setPlaceholder(name, value) {
    /*
    Set the contents of a placeholder element.
    A placeholder is denoted as <i class="placeholder" name="myPlaceholder"></i>
    */
   
    for (const placeholder of document.querySelectorAll(`i.placeholder[name="${name}"]`)) {
        placeholder.innerText = value
    }
}
