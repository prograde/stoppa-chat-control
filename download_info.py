from rich import print

import urllib.request
import json
import os


class Person:
    """
    Information of en person i Riksdagen
    """

    def __init__(self, data: dict) -> None:
        self.data = data

    @property
    def namn(self) -> str:
        """
        Hela personens namn; tilltalsnamn och efternamn
        """
        return f'{self.data["tilltalsnamn"]} {self.data["efternamn"]}'

    @property
    def epostadress(self) -> str:
        """
        Deras officiella e-postadress
        """
        for uppgift in self.data['personuppgift']['uppgift']:
            if uppgift['kod'] == 'Officiell e-postadress':
                return uppgift['uppgift'][0].replace('[på]', '@')
        else:
            raise KeyError(f'E-postadress fattas för: {self.namn}')

    @property
    def parti(self) -> str:
        """
        Förkortning för vilket parti personen tillhör

        Exempel:
            S för Socialdemokraterna
            C för Centerpartiet
            SD för Sverigedemokraterna
            etc.
        """
        return self.data['parti']
        
    @property
    def organ_koder(self) -> set[str]:
        """
        Organkoder för uppdragen personen har
        """
        return set(uppdrag['organ_kod'] for uppdrag
                    in self.data['personuppdrag']['uppdrag'])

    @property
    def roll_koder(self) -> set[str]:
        """
        Rollkoder för uppdragen personen har
        """
        return set(uppdrag['roll_kod'] for uppdrag
                    in self.data['personuppdrag']['uppdrag'])

    @property
    def is_partiledare(self) -> bool:
        """
        Är denna person en partiledare?
        """

        # if self.data['sorteringsnamn'] == 'Andersson,Magdalena':
        #     print(self.data['personuppdrag']['uppdrag'])

        for uppdrag in self.data['personuppdrag']['uppdrag']:
            if uppdrag['roll_kod'] == 'Partiledare':
                return True
        return False



# Ladda ner personlista från Riksdagens API.
url = 'https://data.riksdagen.se/personlista/?utformat=json'

with urllib.request.urlopen(url) as response:
    data = json.loads(response.read())
    # personer = list(map(Person, data['personlista']['person']))
    personer = [Person(person) for person in data['personlista']['person']]
    print('[green]Personlista nedladdad.[/green]\n')


# Försök ladda in manuellt tillagda data.
try:
    with open('medlemslista.json', 'r') as f:
        medlemslista = json.load(f)
except (FileNotFoundError, json.JSONDecodeError):
    medlemslista = {}

# Skapa lista av alla medlemmar som är med
# i en av följande eller är partiledare.
# 
# - EU-nämnden (EUN)
# - Konstitutionsutskottet (KU)
# - Justitieutskottet (JuU)
# 
# Organisera dessa efter parti.

for person in personer:
    if not set(['EUN', 'KU', 'JuU']) & person.organ_koder \
    and 'Partiledare' not in person.roll_koder:
        continue

    if person.parti not in medlemslista:
        medlemslista[person.parti] = []

    medlemslista[person.parti].append(person)


# Checka ifall partiledare finns för varje parti.
for parti, medlemmar in medlemslista.items():
    if not any(medlem.is_partiledare for medlem in medlemmar):
        print(f'[red]Partiledare fattas för [b]{parti}[/b].[/red]')
        print(f'  [yellow]Lägg till detta manuellt i [code]medlemslista.json[/code].[/yellow]')
        print(f'  [yellow i]Detta felet uppstår även om du redan inkluderat dem i filen.[/yellow i]')
        print()

# Konvertera till JSON och skriv till fil.
os.makedirs('public/data', exist_ok=True)
with open('public/data/riksdagen.json', 'w') as f:
    medlemslista_json = {parti: [] for parti in medlemslista.keys()}

    for parti, medlemmar in medlemslista.items():
        for i, medlem in enumerate(medlemmar):
            medlemslista_json[parti].append({
                'namn': medlem.namn,
                'epostadress': medlem.epostadress,
            })

        # Sortera listan i alfabetisk ordning av namnen
        medlemslista_json[parti] = sorted(medlemslista_json[parti],
                                          key=lambda m: m['namn'])

    json.dump(medlemslista_json, f, indent=4)
